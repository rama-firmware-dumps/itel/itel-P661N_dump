## FIRMWARE DUMP
### sys_tssi_64_armv82_itel-user 13 TP1A.220624.014 659554 release-keys
- Transsion Name: itel P55 5G
- TranOS Build: P661N-H334IJKL-T-GL-240716V87
- TranOS Version: itel13.0.0
- Brand: ITEL
- Model: itel-P661N
- Platform: mt6833 (Unknown)
- Android Build: TP1A.220624.014
- Android Version: 13
- Kernel Version: 4.19.191
- Security Patch: 2024-07-05
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: true
- Screen Density: 320
